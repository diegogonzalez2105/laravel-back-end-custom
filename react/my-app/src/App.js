import React, { useState, useEffect } from 'react';
import { MapContainer, TileLayer, Marker, Popup } from 'react-leaflet';
import SearchBar from './SearchBar'; // Ajusta la ruta según la ubicación de tus archivos
import 'leaflet/dist/leaflet.css';

const MapComponent = () => {
  const [center, setCenter] = useState([51.505, -0.09]); // Coordenadas del centro del mapa
  const [markers, setMarkers] = useState([]);
  const [userLocation, setUserLocation] = useState(null);

  // Obtiene la ubicación del usuario cuando el componente se monta
  useEffect(() => {
    navigator.geolocation.getCurrentPosition(
      (position) => {
        const { latitude, longitude } = position.coords;
        setUserLocation([latitude, longitude]);
        setCenter([latitude, longitude]);
      },
      (error) => {
        console.error('Error al obtener la ubicación:', error);
      }
    );
  }, []);

  // Función para manejar la búsqueda de lugares
  const handleSearch = (latitude, longitude) => {
    setUserLocation([latitude, longitude]);
    setCenter([latitude, longitude]);
    setMarkers([]);
  };

  return (
    <div>
      {/* Componente de búsqueda de lugares */}
      <SearchBar onSearch={handleSearch} />

      {/* Mapa con marcadores */}
      <MapContainer center={center} zoom={13} style={{ height: '400px' }}>
        {/* Agrega el componente TileLayer para mostrar el mapa de OpenStreetMap */}
        <TileLayer
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
          attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        />

        {/* Marcador de la ubicación del usuario */}
        {userLocation && (
          <Marker position={userLocation}>
            <Popup>Tu ubicación actual</Popup>
          </Marker>
        )}

        {/* Otros marcadores */}
        {markers.map((marker) => (
          <Marker position={[marker.lat, marker.lng]} key={marker.name}>
            <Popup>{marker.name}</Popup>
          </Marker>
        ))}
      </MapContainer>
    </div>
  );
};

export default MapComponent;
