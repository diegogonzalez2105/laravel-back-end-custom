import React, { useState } from 'react';
import axios from 'axios'; // Importa axios para realizar solicitudes HTTP

const SearchBar = ({ onSearch }) => {
  const [searchText, setSearchText] = useState('');

  const handleSearch = async () => {
    try {
      // Realiza una solicitud a la API de geocodificación (Nominatim de OpenStreetMap)
      const response = await axios.get(`https://nominatim.openstreetmap.org/search?format=json&q=${searchText}`);

      if (response.data.length > 0) {
        // Si se encontraron resultados, toma las coordenadas del primer resultado
        const latitude = parseFloat(response.data[0].lat);
        const longitude = parseFloat(response.data[0].lon);

        // Llama a la función onSearch y pasa las coordenadas de búsqueda
        onSearch(latitude, longitude);
      } else {
        console.log('No se encontraron resultados');
      }
    } catch (error) {
      console.error('Error al buscar lugar:', error);
    }
  };

  return (
    <div>
      <input
        type="text"
        placeholder="Buscar lugar..."
        value={searchText}
        onChange={(e) => setSearchText(e.target.value)}
      />
      <button onClick={handleSearch}>Buscar</button>
    </div>
  );
};

export default SearchBar;
